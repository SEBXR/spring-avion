package gestion.avion;

public class Compagnie{
    private int id;
    private String nom;
    private String mdp;
    private String token;


    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public Compagnie() {
    }

    public Compagnie(int id, String nom, String mdp) {
        this.id = id;
        this.nom = nom;
        this.mdp = mdp;
        this.token = token;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMdp() {
        return this.mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

}