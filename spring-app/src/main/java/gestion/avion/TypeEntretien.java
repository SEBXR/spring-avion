package gestion.avion;

public class TypeEntretien{
    private int id;
    private String libelle;

    public TypeEntretien() {
    }

    public TypeEntretien(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}