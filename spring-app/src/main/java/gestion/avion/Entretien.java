package gestion.avion;

import java.util.Date;

public class Entretien {
    private int id;
    private int idAvion;
    private int idTypeEntretien;
    private Date dateEntretien;


    public Entretien() {
    }

    public Entretien(int id, int idAvion, int idTypeEntretien, Date dateEntretien) {
        this.id = id;
        this.idAvion = idAvion;
        this.idTypeEntretien = idTypeEntretien;
        this.dateEntretien = dateEntretien;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAvion() {
        return this.idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    public int getIdTypeEntretien() {
        return this.idTypeEntretien;
    }

    public void setIdTypeEntretien(int idTypeEntretien) {
        this.idTypeEntretien = idTypeEntretien;
    }

    public Date getDateEntretien() {
        return this.dateEntretien;
    }

    public void setDateEntretien(Date dateEntretien) {
        this.dateEntretien = dateEntretien;
    }

}