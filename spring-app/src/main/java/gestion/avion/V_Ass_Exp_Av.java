package gestion.avion;

import java.util.Date;

public class V_Ass_Exp_Av{
    String modele;
    String photo;
    Date datePaiement;
    Date dateExpiration;
    int expiration;
    String assurance;

    public V_Ass_Exp_Av() {
    }

    public V_Ass_Exp_Av(String modele, String photo, Date datePaiement, Date dateExpiration, int expiration, String assurance) {
        this.modele = modele;
        this.photo = photo;
        this.datePaiement = datePaiement;
        this.dateExpiration = dateExpiration;
        this.expiration = expiration;
        this.assurance = assurance;
    }

    public String getModele() {
        return this.modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDatePaiement() {
        return this.datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Date getDateExpiration() {
        return this.dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public int getExpiration() {
        return this.expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public String getAssurance() {
        return this.assurance;
    }

    public void setAssurance(String assurance) {
        this.assurance = assurance;
    }
    
}