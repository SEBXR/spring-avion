package gestion.avion;

import java.util.Date;

public class ExpirationAssurance{
    private int  id;
    private int  idAssurance;
    private Date datePaiement;
    private Date dateExpiration;
    private int  idAvion;

    public ExpirationAssurance() {
    }

    public ExpirationAssurance(int id, int idAssurance, Date datePaiement, Date dateExpiration, int idAvion) {
        this.id = id;
        this.idAssurance = idAssurance;
        this.datePaiement = datePaiement;
        this.dateExpiration = dateExpiration;
        this.idAvion = idAvion;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAssurance() {
        return this.idAssurance;
    }

    public void setIdAssurance(int idAssurance) {
        this.idAssurance = idAssurance;
    }

    public Date getDatePaiement() {
        return this.datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Date getDateExpiration() {
        return this.dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public int getIdAvion() {
        return this.idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

}