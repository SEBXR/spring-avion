package gestion.avion;

import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@SpringBootApplication
public class GestionAvionApplication {
	@Autowired
    private JdbcTemplate jdbcTemplate;  
	private GenerateToken generate;

	public static void main(String[] args) {
		SpringApplication.run(GestionAvionApplication.class, args);
	}

	@RequestMapping("/")
	public String index(){
		return "BIENVENUE SUR NOTRE SPRING BOOT";
	}

	//Princy
	@RequestMapping(method =RequestMethod.GET ,value={"/detailsavions","/detailsavions/{id}"})
	@CrossOrigin
	public List<Avion> listesAvion(@PathVariable (required = false) Integer id){
		String sql = String.format("SELECT * FROM V_model_avions ");
		if(id != null){
			sql = sql.concat("WHERE id = "+ id);
			return  jdbcTemplate.query(sql,new 	BeanPropertyRowMapper(Avion.class));
		}
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper (Avion.class));
	}

	//Etienne
	@RequestMapping(method =RequestMethod.GET ,value={"/avionsAssurance","/avionsAssurance/{id}"})
	@CrossOrigin
	public List<Avion> Avion(@PathVariable (required = false) Integer id){
		String sql = String.format("SELECT * FROM V_model_av ");
		if(id != null){
			sql = sql.concat("WHERE id = "+ id);
			return  jdbcTemplate.query(sql,new 	BeanPropertyRowMapper(Avion.class));
		}
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper (Avion.class));
	}


	@GetMapping("/assurances")
	public List<Assurance> listAssurance(){
		String sql = String.format("SELECT * FROM Assurance");
		return jdbcTemplate.query(sql,new BeanPropertyRowMapper(Assurance.class));
	}

	@GetMapping("/ass_Expire")
	public List<V_Ass_Exp_Av> listeAssExpire(){
		String sql = String.format("SELECT * FROM V_Ass_Exp_Av");
		return  jdbcTemplate.query(sql,new BeanPropertyRowMapper (V_Ass_Exp_Av.class));
	}

	@GetMapping("/ass_Expire1Mois")
	public List<V_Ass_Exp_Av> listeAssExpire1(){
		String sql = String.format("SELECT * FROM V_Ass_Exp_Av where expiration <= 1");
		return  jdbcTemplate.query(sql,new BeanPropertyRowMapper (V_Ass_Exp_Av.class));
	}

	@GetMapping("/ass_Expire3Mois")
	public List<V_Ass_Exp_Av> listeAssExpire3(){
		String sql = String.format("SELECT * FROM V_Ass_Exp_Av where expiration <= 3");
		return  jdbcTemplate.query(sql,new BeanPropertyRowMapper (V_Ass_Exp_Av.class));
	}

	@PostMapping("/login")
	@CrossOrigin 
	public String Login(@RequestParam(value="nom",defaultValue="") String nom, @RequestParam(value="mdp",defaultValue="") String mdp) {
		String sql = "select*from Compagnie where nom ='"+nom+"' and mdp = '"+mdp+"'";
		
		List<Compagnie> verif = jdbcTemplate.query(sql,new BeanPropertyRowMapper(Compagnie.class));
		if(verif.size()>0){
			String token = generate.genererToken(nom);
			String query = "update Compagnie set token = '"+token+"' where nom = '"+nom+"' and mdp = '"+mdp+"'";
		    jdbcTemplate.update(query);
			return token;

		}
		return "Non connecte";
	}

	
}
