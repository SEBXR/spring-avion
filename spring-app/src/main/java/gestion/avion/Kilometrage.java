package gestion.avion;

import java.util.Date;

public class Kilometrage{
    private int idAvion;
    private Date date;
    private int debut_km;
    private int fin_km;

    public Kilometrage() {
    }

    public Kilometrage(int idAvion, Date date, int debut_km, int fin_km) {
        this.idAvion = idAvion;
        this.date = date;
        this.debut_km = debut_km;
        this.fin_km = fin_km;
    }

    public int getIdAvion() {
        return this.idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDebut_km() {
        return this.debut_km;
    }

    public void setDebut_km(int debut_km) {
        this.debut_km = debut_km;
    }

    public int getFin_km() {
        return this.fin_km;
    }

    public void setFin_km(int fin_km) {
        this.fin_km = fin_km;
    }

}