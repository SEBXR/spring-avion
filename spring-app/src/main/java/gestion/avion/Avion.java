package gestion.avion;

public class Avion{
    private int id;
    private String modele;
    // private int idMarqueAvion;
    private String Compagnie;
    private String photo;

    public Avion() {
    }

    public Avion(int id, String modele, String Compagnie, String photo) {
        this.id = id;
        this.modele = modele;
        // this.idMarqueAvion = idMarqueAvion;
        this.Compagnie = Compagnie;
        this.photo = photo;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModele() {
        return this.modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getCompagnie() {
        return this.Compagnie;
    }

    public void setCompagnie(String Compagnie) {
        this.Compagnie = Compagnie;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}