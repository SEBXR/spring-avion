package gestion.avion;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mandrs
 */

public class GenerateToken {

    /**
     * @param args the command line arguments
     */
    private static String Sha1(String password) throws UnsupportedEncodingException {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
    public static String genererToken(String user){
        String name=user;
        LocalDateTime ajd=LocalDateTime.now();
        Integer Y=ajd.getYear();
            String taona=Y.toString();
        Integer m=ajd.getMonthValue();
            String volana=m.toString();
        Integer d=ajd.getDayOfMonth();
            String andro=d.toString();
        Integer min=ajd.getMinute();
            String lera=min.toString();
            String hash="";
        try {
            String hash1=GenerateToken.Sha1(name);
            String h2=GenerateToken.Sha1(taona+volana+andro+lera);
            hash=GenerateToken.Sha1(hash1+h2);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GenerateToken.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hash;
    }
//     public static Boolean check_Token(String exToken,String user){
//         Boolean v=false;
//         if(exToken.equals(GenerateToken.genererToken("test"))){
//            v=true;
//         }
//         return v;
//     } 

//     public static void main(String[] args) {
//         String bla=GenerateToken.genererToken("test");
//         System.out.println(bla);
//         String ex="9bd7c501080bc07ce00add929ef0f72f08c5c01d";
//         System.out.print(check_Token(ex,"test"));
        
//     }
//    //0203d30f6c1ea8e3bf19117f6412cf7b7f493879
    
}
