package gestion.avion;

public class Assurance{
    private int id;
    private String nom;
    private int montantAssurance;

    public Assurance() {
    }

    public Assurance(int id, String nom, int montantAssurance) {
        this.id = id;
        this.nom = nom;
        this.montantAssurance = montantAssurance;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getMontantAssurance() {
        return this.montantAssurance;
    }

    public void setMontantAssurance(int montantAssurance) {
        this.montantAssurance = montantAssurance;
    }

}